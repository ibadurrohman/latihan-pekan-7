@extends('layout.master')

@section('title', 'Edit Data Pemain Film')

@section('content')
<form action="/cast/{{$post->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" value="{{$post->nama}}" id="nama" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">umur</label>
        <input type="text" class="form-control" name="umur"  value="{{$post->umur}}"  id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">bio</label>
        <input type="text" class="form-control" name="bio"  value="{{$post->bio}}"  id="bio" placeholder="Masukkan Bio">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection
    
